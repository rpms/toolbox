Name:		toolbox
Version:	0.0.7
Release:	1%{?dist}
Summary:	Script to launch privileged container with podman

License:	ASL 2.0
URL:		https://github.com/coreos/toolbox
Source0:	https://github.com/coreos/%{name}/archive/%{version}.tar.gz
Requires:	podman
BuildArch:	noarch

%description
toolbox is a small script that launches a container to let
you bring in your favorite debugging or admin tools.

%define debug_package %{nil}

%prep
%autosetup

%build
# No building required

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT/%{_bindir}
install -m 755 rhcos-toolbox $RPM_BUILD_ROOT/%{_bindir}/toolbox

%files
%license LICENSE
%doc README.md NOTICE
%{_bindir}/toolbox


%changelog
* Tue Mar 24 2020 Jindrich Novy <jnovy@redhat.com> - 0.0.7-1
- New upstream release 0.0.7
- Resolves: #1816287

* Fri Jun 14 2019 Yu Qi Zhang <jerzhang@redhat.com> - 0.0.4-1.el8
- Update for rhel8.1 container-tools module

* Tue May 21 2019 Steve Milner <smilner@redhat.com> - 0.0.4-1.rhaos4.2.el8
- Add help switch per RHBZ#1684258
- Spec fixes found by rpmlint

* Thu May 2 2019 Micah Abbott <miabbott@redhat.com> - 0.0.3-1.rhaos4.1.el8
- Use rhel8/support-tools

* Sat Jan 26 2019 Yu Qi Zhang <jerzhang@redhat.com> - 0.0.2-1.rhaos4.1.el8
- Add runlabel options and fix default image

* Thu Sep 6 2018 Yu Qi Zhang <jerzhang@redhat.com> - 0.0.1-1.rhaos4.1.el8
- Initial Specfile for Red Hat CoreOS Toolbox
